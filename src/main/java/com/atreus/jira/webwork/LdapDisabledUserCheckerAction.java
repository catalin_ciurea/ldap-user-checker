package com.atreus.jira.webwork;

import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.web.action.JiraWebActionSupport;

/**
 * 07/04/2015 12:36 AM
 * Created by catalin.
 */
public class LdapDisabledUserCheckerAction extends JiraWebActionSupport {

    private int nrActiveUsers = 0;
    private UserUtil userUtil;

    public LdapDisabledUserCheckerAction(UserUtil userUtil) {
        this.userUtil = userUtil;
    }

    @Override
    protected String doExecute() throws Exception {
        nrActiveUsers = userUtil.getActiveUserCount();
        return SUCCESS;
    }

    public int getNrActiveUsers() {
        return nrActiveUsers;
    }
}
